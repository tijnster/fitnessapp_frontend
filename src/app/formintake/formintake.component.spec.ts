import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormintakeComponent } from './formintake.component';

describe('FormintakeComponent', () => {
  let component: FormintakeComponent;
  let fixture: ComponentFixture<FormintakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormintakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormintakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
