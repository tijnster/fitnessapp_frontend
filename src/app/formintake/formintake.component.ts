import { Component, OnInit } from "@angular/core";
import { User } from "../user";
import { UserService } from "../user.service";

@Component({
  selector: "app-formintake",
  templateUrl: "./formintake.component.html",
  styleUrls: ["./formintake.component.css"]
})
export class FormintakeComponent implements OnInit {
  genders = ["man", "woman"];
  model = new User();
  public data: any;
  showActions: boolean = false;

  constructor(private userService: UserService) {}

  ngOnInit() {}

  onCalculateTdee() {
    this.userService.createUser(this.model).subscribe(
      response => {
        this.data = response;
        console.log(this.data);
        console.log(this.data.bmr);
        this.showActions = true;
      },
      error => console.log(error)
    );
  }
}
