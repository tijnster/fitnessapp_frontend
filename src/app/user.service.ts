import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Macroresponse } from "./macroresponse";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private baseUrl = "http://localhost:8080/api/v1/tdee";

  constructor(private http: HttpClient) {}

  getEmployee(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createUser(user: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, user);
  }

  // createUser(user: Object): Observable<Object> {
  //   return this.http.post(`${this.baseUrl}`, user).pipe(
  //     map((data: Macroresponse[]) => {
  //       return data;
  //     }),
  //     catchError(error => {
  //       return throwError("Something went wrong!");
  //     })
  //   );
  // }

  updateEmployee(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: "text" });
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
