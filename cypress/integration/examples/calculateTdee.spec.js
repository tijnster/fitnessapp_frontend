describe("Fit app Cypress tests", function() {
  beforeEach(function() {
    // cy.fixture('stub').as('stub');
    cy.visit("http://localhost:4200");
  });

  it("Calculating TDEE", function() {
    cy.server();
    cy.route({
      method: "POST",
      url: "http://localhost:8080/api/v1/tdee",
      status: 200,
      response: {
        name: null,
        gender: null,
        weight: 0,
        length: 0,
        age: 0,
        activity: 0.0,
        goal: null,
        bmr: 457,
        amountOfProteines: 45,
        amountOfCarbs: 45,
        amountOfFats: 10,
        percentageProteins: 40,
        percentageCarbs: 40,
        percentageFats: 20,
        carbEnergy: 182,
        proteinEnergy: 182,
        fatEnergy: 91,
        id: 0
      }
    });

    cy.get("#mat-input-0").type("Martijn");
    cy.get("#mat-radio-3-input").click({ force: true });
    cy.get("#mat-input-1").type("75");
    cy.get("#mat-input-2").type("175");
    cy.get("#mat-input-3").type("28");
    cy.get("#mat-select-0").click({ force: true });
    cy.get("#mat-option-0 > .mat-option-text").click();
    cy.get("#mat-select-1").click();
    cy.get("#mat-option-3 > .mat-option-text").click();
    cy.get(".calculate > .mat-button-wrapper").click();
  });
});
