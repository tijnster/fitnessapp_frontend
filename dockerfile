# base image
FROM node:12.2.0 AS builder

# set working directory
WORKDIR /my-food-app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# add app
COPY . /my-food-app

# install
RUN npm install
RUN $(npm bin)/ng build --prod

FROM nginx:1.15.8-alpine
COPY --from=builder /my-food-app/dist/my-food-app/ /usr/share/nginx/html
